variable "count" {
  description = "Number of instances"
  default = 3
}
variable "name" {}
variable "ami" {}
variable "instance_type" {
  default = "t2.micro"
}
variable "subnet_id" {}
variable "sg_id" {}
variable "key_name" {}
variable "public_keyfile" {}
variable "availability_zone" {}
variable "launch_script" {}
