resource "aws_instance" "app" {
  count = "${var.count}"
  ami = "${var.ami}"
  availability_zone = "${var.availability_zone}"
  instance_type="${var.instance_type}"
  key_name = "${aws_key_pair.this.key_name}"
  subnet_id="${var.subnet_id}"
  vpc_security_group_ids=["${var.sg_id}"]
  associate_public_ip_address = true
  user_data = "${file(var.launch_script)}"


  tags {
    Name="${var.name}-${count.index}"
    Group="${var.name}"
  }

  depends_on = ["aws_key_pair.this"]
}

resource "aws_key_pair" "this" {
  key_name = "${var.key_name}"
  public_key = "${file(var.public_keyfile)}"
}
