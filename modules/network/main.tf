resource "aws_vpc" "this" {
  cidr_block="10.0.0.0/16"
  enable_dns_hostnames = true


  tags {
    Name = "${var.vpc_name}"
  }
}

/*
  Public Subnet
*/

resource "aws_subnet" "ap-southeast-1a-public" {
  vpc_id="${aws_vpc.this.id}"
  cidr_block="10.0.1.0/24"
  availability_zone = "ap-southeast-1a"

  tags {
    Name = "public subnet"
  }
}

resource "aws_route_table" "ap-southeast-1a-public" {
    vpc_id = "${aws_vpc.this.id}"

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.this.id}"
    }

    tags {
        Name = "public Subnet"
    }
}

resource "aws_route_table_association" "ap-southeast-1a-public" {
    subnet_id = "${aws_subnet.ap-southeast-1a-public.id}"
    route_table_id = "${aws_route_table.ap-southeast-1a-public.id}"
}

resource "aws_internet_gateway" "this" {
    vpc_id = "${aws_vpc.this.id}"
}

resource "aws_security_group" "web" {
  name="web"
  description="Allow all inbound and outbound traffic within VPC and SSH from internet"
  vpc_id="${aws_vpc.this.id}"

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port=0
    to_port=0
    protocol="-1"
    cidr_blocks=["0.0.0.0/0"]
  }

  egress {

    from_port=0
    to_port=0
    protocol="-1"
    cidr_blocks=["0.0.0.0/0"]

  }
}
