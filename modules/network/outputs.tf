output "vpc_id" {
  value = "${element(concat(aws_vpc.this.*.id, list("")), 0)}"

}

output "subnet_id" {
  value = "${aws_subnet.ap-southeast-1a-public.id}"

}

output "sg_id" {
  value = "${aws_security_group.web.id}"
}
