#!/bin/bash

for i in $(sed -n '3{p;q}' output.log | sed "s/,/ /g"); do ssh -o StrictHostKeyChecking=no  -i ~/jmeter-ec2/keys/jmeter-provisioner ubuntu@$i 'nohup sh run-slave.sh > /dev/null 2>&1' & done;
