#!/bin/bash

OUT="result/${1:-out.csv}"
ssh -o StrictHostKeyChecking=no jmeter-master 'sh jmeter-master/run.sh' && \
rsync -avzr ubuntu@jmeter-master:~/jmeter-master/out.csv $OUT
