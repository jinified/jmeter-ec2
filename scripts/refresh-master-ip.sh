#!/bin/bash

HOST="jmeter-master"
IP=$1
OLDIP=`grep -w jmeter-master -A 1 ~/.ssh/config | awk '/HostName/ {print $2}'`
echo "OLD IP: " $OLDIP
echo "NEW IP: " $IP
sed -i "s/$OLDIP/$IP/g" ~/.ssh/config

sed -i "/$HOST/ s/.*/$IP\t$HOST/g" /etc/hosts



