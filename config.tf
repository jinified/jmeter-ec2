variable "region" {
    default = "us-east-1"
}
variable "vpc_name" {
    default = "my_vpc"
}
variable "count" {
    default = 3
}
variable "name" {}
variable "ami" {}
variable "instance_type" {
    default = "t2.micro"
}
variable "key_name" {}
variable "public_keyfile" {}
variable "availability_zone" {
  default = "ap-southeast-1a"
}
variable "launch_script" {}
variable "bucket_name" {}
