#!/bin/bash
N_THREADS=100
RAMPUP=100
HOSTS=$(cat /home/ubuntu/jmeter-master/hosts.txt)

# Allocate sufficient memory for Jmeter
export JVM_ARGS="-Xms2g -Xmx10g"

cd $HOME/apache-jmeter-4.0/bin
./jmeter.sh -n -t $HOME/jmeter-master/apache-jmeter-4.0/bin/test.jmx -l $HOME/jmeter-master/out.csv -DTHREADS=$N_THREADS -DRAMP-UP=$RAMPUP -R $HOSTS
