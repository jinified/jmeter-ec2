# JMeter Distributed Load Testing with AWS

## Pre-requisite

1. Have access to jumpstart host. Please obtain the required SSH key
    - IP: 13.251.76.123
    - Instance ID: i-087fa9707790dea1f

## Setting up the jump host

1. Install `terraform` [here](https://www.terraform.io/intro/getting-started/install.html)
2. Install `packer` [here](https://www.packer.io/intro/getting-started/install.html)
3. Install `jq` for JSON processing [here](https://stedolan.github.io/jq/)
4. Install latest `awscli`
```bash
sudo apt-get install python-pip
pip install awscli --upgrade --user
echo "export PATH=/home/ubuntu/.local/bin:$PATH" >> ~/.bashrc
```
5. Install make
```bash
sudo apt-get install make
```
6. Copy ssh config
```bash
cp ~/jmeter-ec2/scripts/ssh-config ~/.ssh/config
```
7. Install amazon-ssm-agent
```bash
wget https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/debian_amd64/amazon-ssm-agent.deb
sudo dpkg -i amazon-ssm-agent.deb
```
7. Install and setup dsh
```bash
sudo apt-get install dsh
cp -r ~/jmeter-ec2/dsh ~/.dsh
```
8. Clone repository [git@bitbucket.org:jinified/jmeter-ec2.git](git@bitbucket.org:jinified/jmeter-ec2.git) to `home/ubuntu`

## One-time setup

1. make clean
2. make gen-keys
3. make bake
4. Ensures that ami in `terraform.tfvars` is updated with ami-ID from `output.log`
5. terraform get && terraform init

## Deploying AWS Infrastructure

- terraform plan
> Review what will be changed and added to current infrastructure setup
- terraform apply
- configure appropriate options in `config.tf`
> information about infrastructure can be found in `output.log`
> use sed -n '<line-number>{p;q}' output.log to obtain specific line in output.log

## Post-setup

### Add master instance to SSH_CONFIG

- run `scripts/refresh-master-ip.sh $(sed -n '2{p;q}' output.log')
- sync running directory to **jmeter-master**
```bash
rsync -avzr jmeter-master ubuntu@jmeter-master:~/ --info=progress2
```

---

## General Workflow

- SSH to jumpstart machine
- Make sure `jmeter-ec2` is fully updated by running a `git pull --rebase origin master`
- Set up architecture (update if any changes required)
```bash
# To deploy custom AMI
make bake
# To deploy your plan after any changes
make all
```
- Sync master-instance working folder
```bash
rsync -avzr jmeter-master ubuntu@jmeter-master:~/
```
- Run all slaves
```bash
sh scripts/run-slave.sh
```
- Run master instance
```bash
sh scripts/run-master.sh
```
- Connect to master instance with GUI
```bash
ssh -X ubuntu@jmeter-master 'sh jmeter-master/apache-jmeter-4.0/bin/jmeter.sh'
```

---

## Known Issues

- [User-data script is not running on custom AMI]https://stackoverflow.com/questions/27086639/user-data-scripts-is-not-running-on-my-custom-ami-but-working-in-standard-amazo)

---

## Useful shell commands

### Describe all EC2 instances according to tag

```bash
aws ec2 describe-instances --filters Name=tag:Group,Values=jmeter-test
```

### Run command on multiple EC2 instances

#### Vanilla SSH way

```bash
for i in $(sed -n '3{p;q}' output.log | sed "s/,/ /g"); do ssh -o StrictHostKeyChecking=no  -i ~/jmeter-ec2/keys/jmeter-provisioner ubuntu@$i 'sh run-slave.sh' & done
```

#### [PDSH](https://gist.github.com/mbbx6spp/c16b5438270be609619f83c462b148d5)

### SSM Command

```bash
aws ssm send-command --targets "Key=Group,Values=jmeter-test" --document-name "AWS-RunShellScript" --comment "Run slave machine" --parameters "commands=["sh run-slave.sh"]" --output text
```
