SSH_KEY_NAME=jmeter-provisioner

.PHONY: clean
clean:
	rm -rf keys

gen-keys:
	mkdir keys
	ssh-keygen -t rsa \
		-C jmeter-provisioner \
		-f keys/$(SSH_KEY_NAME) \
		-m pem \
		-P ''
	aws ec2 import-key-pair  --key-name $(SSH_KEY_NAME) --public-key-material file://keys/$(SSH_KEY_NAME).pub

bake: ./packer/jmeter-slaves.json ./packer/vars.json
	rm -f output.log
	packer build -force -machine-readable -var-file packer/vars.json packer/jmeter-slaves.json | tee packer-build.log
	grep 'artifact,0,id' packer-build.log | cut -d, -f6 | cut -d: -f2 >> output.log

	
build: terraform.tf
	sed -i "s/$(shell grep 'ami' terraform.tfvars | cut -d= -f2)/$(shell head -n1 output.log)/" terraform.tfvars;\
	terraform plan && terraform apply

post:
	sed -ni '1p' output.log
	terraform output -json -module=ec2 | jq -r '.public_ips.value[0]' >> output.log
	terraform output -module=ec2 -json | jq -r '.public_ips.value[1:] | @csv' | sed 's/"//g' >> output.log
	terraform output -module=ec2 -json | jq -r '.private_ips.value[1:] | @csv' | sed 's/"//g' >> output.log
	sed -n '4{p;q}' output.log > jmeter-master/hosts.txt
	sudo sh scripts/refresh-master-ip.sh $(shell terraform output -json -module=ec2 | jq -r '.public_ips.value[0]')

refresh: clean gen-keys
all: build post
