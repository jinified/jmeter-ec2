provider "aws" {
  version = ">= 1.2.0"
  region = "${var.region}"
}

module "net" {
  source = "./modules/network"
  vpc_name = "${var.vpc_name}"
}

module "ec2" {
  source = "./modules/compute"
  count  = "${var.count}"
  name  = "${var.name}"
  ami  = "${var.ami}"
  availability_zone = "${var.availability_zone}"
  instance_type  = "${var.instance_type}"
  key_name = "${var.key_name}"
  public_keyfile = "${var.public_keyfile}"
  launch_script = "${var.launch_script}"
  subnet_id  = "${module.net.subnet_id}"
  sg_id  = "${module.net.sg_id}"
}

resource "aws_s3_bucket" "this" {
  bucket = "${var.bucket_name}"
  region = "${var.region}",
  acl    = "private"
}
